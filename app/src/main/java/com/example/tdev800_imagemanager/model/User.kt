package com.example.tdev800_imagemanager.model
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") val id: Int,
    @SerializedName("username") val name: String,
    @SerializedName("email") val login: String,
    @SerializedName("password") val avatarUrl: String,
    @SerializedName("age") val age: Int,
    @SerializedName("role") val role: Int


)
