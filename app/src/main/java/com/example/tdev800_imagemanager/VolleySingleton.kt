package com.example.tdev800_imagemanager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.example.tdev800_imagemanager.view.DashboardFragment
import com.example.tdev800_imagemanager.view.LoginFragment
import com.example.tdev800_imagemanager.view.RegisterFragment


class VolleySingleton(context: RegisterFragment) {
    companion object {
        @Volatile
        private var INSTANCE: VolleySingleton? = null
        fun getInstance(context: RegisterFragment) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: VolleySingleton(context).also {
                    INSTANCE = it
                }
            }
    }
    private val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.context)
    }
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}

class VolleySingleton2(context: LoginFragment) {
    companion object {
        @Volatile
        private var INSTANCE: VolleySingleton2? = null
        fun getInstance(context: LoginFragment) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: VolleySingleton2(context).also {
                    INSTANCE = it
                }
            }
    }
    private val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.context)
    }
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}


class VolleySingleton3(context: DashboardFragment) {
    companion object {
        @Volatile
        private var INSTANCE: VolleySingleton3? = null
        fun getInstance(context: DashboardFragment) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: VolleySingleton3(context).also {
                    INSTANCE = it
                }
            }
    }
    private val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.context)
    }
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}