
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tdev800_imagemanager.model.User


class UserModel : ViewModel() {
    val username = MutableLiveData<String>()
    val userid = MutableLiveData<Int>()
    val emailaddress = MutableLiveData<String>()
    val photoURI = MutableLiveData<Uri>()
    val request = MutableLiveData<String>()

    fun setUsername(msg: String) {
        username.value = msg.capitalize()
    }


    fun setUserId(msg: Int) {
        userid.value = msg
    }

    fun setEmailAddress(msg: String) {
        emailaddress.value = msg
    }

    fun setPhotoURI(msg: Uri){
        photoURI.value = msg
    }

    fun setGsURI(msg: String){
        request.value = msg
    }
}