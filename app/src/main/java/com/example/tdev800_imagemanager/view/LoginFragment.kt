package com.example.tdev800_imagemanager.view

import UserModel
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.tdev800_imagemanager.R
import com.example.tdev800_imagemanager.VolleySingleton2
import com.example.tdev800_imagemanager.model.User
import org.json.JSONObject
import java.time.LocalDateTime


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */


class LoginFragment : Fragment() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.login_fragment, container, false)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //important viewModel
        val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)



        view.findViewById<TextView>(R.id.notregisteredyet).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_RegisterFragment)
        }



        view.findViewById<Button>(R.id.login_button).setOnClickListener {


            //values
            val editTextUsernameField = view.findViewById<EditText>(R.id.emailInputEditText)
            val editTextPwdField = view.findViewById<EditText>(R.id.passwordInputEditText)
            //assigning
            val editTextUInput = editTextUsernameField.text.toString()
            val editTextPInput = editTextPwdField.text.toString()

            //volley time
            val url = "https://t-dev-800-backend.herokuapp.com/api/user/username/$editTextUInput"

            //requesting
            val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                Response.Listener { response ->

                    val jObj = JSONObject(response.toString())
                    val pwd = jObj.getString("password").toString()
                    val id = jObj.getInt("id")
                    val email = jObj.getString("email")




                    Log.i("pwd input", editTextPInput)
                    Log.i("jsonobject","Response: %s".format(jObj ))
                    Log.i("password in db","Response: %s ".format(pwd ))

                    if(editTextPInput == pwd ){
                        model?.setUsername(editTextUInput)
                        model?.setUserId(id)
                        model?.setEmailAddress(email)

                        //navigation to dashboard
                        findNavController().navigate(R.id.action_FirstFragment_to_dashboard_activity)
                    }
                    else{
                        val text = "Wrong credentials, please try again"
                        val duration = Toast.LENGTH_SHORT

                        val toast = Toast.makeText(context, text, duration)
                        toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER, 0, -100)

                        toast.show()
                        Log.e("wrong credentials","wrong pwd")


                    }


                },
                Response.ErrorListener { error ->
                    Log.e("error:", "seems like there were an issue $error")
                    val text = "Wrong credentials, please try again"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(context, text, duration)
                    toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER, 0, -100)

                    toast.show()
                    Log.e("wrong credentials","wrong pwd")
                }


            )

            // Access the RequestQueue through your singleton class.
            VolleySingleton2.getInstance(this).addToRequestQueue(jsonObjectRequest)




        }
    }
}
