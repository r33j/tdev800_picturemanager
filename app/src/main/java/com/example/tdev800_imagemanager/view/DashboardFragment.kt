package com.example.tdev800_imagemanager.view

import UserModel
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.tdev800_imagemanager.GlideApp
import com.example.tdev800_imagemanager.R
import com.example.tdev800_imagemanager.VolleySingleton3
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@Suppress("DEPRECATION")
class DashboardFragment : Fragment() {
    val REQUEST_TAKE_PHOTO = 1
    val RESULT_CODE = 12


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        })
    }



    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard_fragment, container, false)


    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        val username = view.findViewById<TextView>(R.id.username_dashboard)



        //defining view model
        val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)
        model.username.observe(viewLifecycleOwner,
            Observer<Any> { o -> username.text = o?.toString() })
        //and its values
        val userid =  model.userid.value
        val username_mvvm =  model.username.value?.toLowerCase()




        //Clickable username to user info
        view.findViewById<TextView>(R.id.username_dashboard).setOnClickListener {
            findNavController().navigate(R.id.action_dashboard_activity_to_user_information)

        }

        //User info icon
        view.findViewById<ImageView>(R.id.user_information_logo).setOnClickListener {
            findNavController().navigate(R.id.action_dashboard_activity_to_user_information)

        }




        //TAKING PICTURE FROM CAMERA
        var pm = requireActivity().packageManager



        lateinit var currentPhotoPath: String

        @Throws(IOException::class)
        fun createImageFile(): File {
            // Create an image file name
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val storageDir: File = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
            ).apply {
                // Save a file: path for use with ACTION_VIEW intents
                currentPhotoPath = absolutePath
            }

        }


         fun dispatchTakePictureIntent() {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(pm)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()

                    } catch (ex: IOException) {

                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri? = context?.applicationContext?.let { it1 ->
                            FileProvider.getUriForFile(
                                it1, "com.example.tdev800_imagemanager.provider", it)
                        }
                        if (photoURI != null) {
                            model?.setPhotoURI(photoURI)
                        }
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO  )


                    }

                }
            }

        }


        // Conditional display TEXT or IMAGELIST GRID attached to uuid
        val storage = Firebase.storage
        val storageRef = storage.reference
        val listRef = storage.reference.child("upload/${username_mvvm}_id${userid}")
        listRef.listAll()
            .addOnSuccessListener { listResult ->
                listResult.prefixes.forEach { prefix ->
                    val ifNoPhoto = view.findViewById<TextView>(R.id.noPictureText)
                    ifNoPhoto.visibility = View.INVISIBLE

                }

                listResult.items.forEach { item ->
                    // All the items under listRef.
                }
            }
            .addOnFailureListener {
                // Uh-oh, an error occurred!
            }

        //Floation Action Button TOGGLE INTO CAMERA CHOICE
        view.findViewById<FloatingActionButton>(R.id.floating_action_button).setOnClickListener{

            MaterialAlertDialogBuilder(context)
                .setTitle(resources.getString(R.string.upload_dialog))
                .setMessage(resources.getString(R.string.upload_header))

                .setNeutralButton(resources.getString(R.string.upload_from_camera)) { dialog, which ->
                    dispatchTakePictureIntent()

                }
                .setPositiveButton(resources.getString(R.string.upload_from_gallery)) { dialog, which ->
                    val galleryIntent = Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(galleryIntent, 1000)              }
                .show()
        }

        view.findViewById<Button>(R.id.uploadBtnTry).setOnClickListener {

        }



    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fun uploadFile() {

            val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)
            val username = model.username.value?.toLowerCase()
            val userid = model.userid.value


            //firebase singleton
            val storage = Firebase.storage
            val storageRef = storage.reference

            // Create a child reference
            // imagesRef now points to "images"
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imagesRef: StorageReference? =
                storageRef.child("uploads/${username}_id${userid}/${username}_id${userid}_at_$timeStamp")

            model.setGsURI(imagesRef.toString())

            val uploadTask = model.photoURI.value.let {
                model.photoURI.value?.let { it1 ->
                    imagesRef?.putFile(
                        it1
                    )
                }
            }
            uploadTask?.addOnFailureListener {
                val text = "Oops there's been an issue with upload"
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(context, text, duration)
                toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER, 0, -100)

                toast.show()
            }?.addOnSuccessListener {
                val text = "Upload success !"
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(context, text, duration)
                toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER, 0, -100)

                toast.show()
            }
            val params = HashMap<String, String>()
            params["initialName"] = "${username}_id${userid}_at_$timeStamp"
            params["description"] = "not desc yet"

            var url = "https://t-dev-800-backend.herokuapp.com/api/createPicture"
            val jsonObject = JSONObject(params as Map<*, *>)

            // Volley post request with parameters
            val request = JsonObjectRequest(
                Request.Method.POST, url, jsonObject,
                Response.Listener { response ->
                    // Process the json
                    try {
                        Log.i("Reponse", "Response: $response")

                    } catch (e: Exception) {
                        Log.e("Exception error", "Exception: $e")
                    }

                }, Response.ErrorListener {
                    // Error in request
                    Log.e("Error", "Volley error: $it")

                })


            // Volley request policy, only one time request to avoid duplicate transaction
            request.retryPolicy = DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                // 0 means no retry
                0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
                1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )

            // Add the volley post request to the request queue
            VolleySingleton3.getInstance(this).addToRequestQueue(request)

        }

        uploadFile()

        val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)
        val request = model.request.value
        val gsReference = request?.let { FirebaseStorage.getInstance().getReferenceFromUrl("$request.jpg") }

        val imageViewDashboard = view?.findViewById<ImageView>(R.id.imageViewDashboard)


        if (imageViewDashboard != null) {
            Log.i("Logging GLIDE: ", request)
            GlideApp.with(this)
                .load(gsReference)
                .into(imageViewDashboard)
            }
        }


}
