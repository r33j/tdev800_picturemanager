package com.example.tdev800_imagemanager.view

import UserModel
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.tdev800_imagemanager.R

class UserInfoFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.user_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //defining value
        val username = view.findViewById<TextView>(R.id.username_info)


        //important viewModel
        val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)
        model?.username?.observe(viewLifecycleOwner,
            Observer<Any> { o -> username.text= o?.toString() })



        //Modify password
        view.findViewById<Button>(R.id.modify_password_button).setOnClickListener{
            findNavController().navigate(R.id.action_user_information_to_modify_password)
        }

        //About app
        view.findViewById<Button>(R.id.about_app_button).setOnClickListener{
            findNavController().navigate(R.id.action_user_information_to_about_app)
        }
        //Help
        view.findViewById<Button>(R.id.help_button).setOnClickListener{
            findNavController().navigate(R.id.action_user_information_to_help)
        }

        //Sign out
        view.findViewById<Button>(R.id.sign_out_button).setOnClickListener {
            findNavController().navigate(R.id.action_user_information_to_FirstFragment)
        }

        //back btn
        view.findViewById<Button>(R.id.backButton).setOnClickListener {
            findNavController().navigate(R.id.action_user_information_to_dashboard_activity)
        }




    }



}