package com.example.tdev800_imagemanager.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.tdev800_imagemanager.R
import com.example.tdev800_imagemanager.VolleySingleton
import org.json.JSONObject


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class RegisterFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        view.findViewById<Button>(R.id.register_button).setOnClickListener {
            var url = "https://t-dev-800-backend.herokuapp.com/api/createUser"

            //placing into values
            val emailInputRegisterField = view.findViewById<EditText>(R.id.emailInputRegister)
            val usernameInputRegisterField = view.findViewById<EditText>(R.id.usernameInputRegister)
            val passwordInputRegisterField = view.findViewById<EditText>(R.id.passwordInputRegister)
            val passwordConfirmInputRegisterField =
                view.findViewById<EditText>(R.id.passwordConfirmInputRegister)
            //assigning
            val emailInputRegister = emailInputRegisterField.text.toString()
            val usernameInputRegister = usernameInputRegisterField.text.toString()
            val passwordInputRegister = passwordConfirmInputRegisterField.text.toString()
            val passwordConfirmInput = passwordConfirmInputRegisterField.text.toString()


            // Post parameters
            // Form fields and values
            val params = HashMap<String, String>()
            params["name"] = usernameInputRegister
            params["email"] = emailInputRegister
            params["password"] = passwordInputRegister


            val jsonObject = JSONObject(params as Map<*, *>)

            if (emailInputRegister != "" && usernameInputRegister != "" && passwordConfirmInput != "" && passwordInputRegister == passwordConfirmInput) {
                // Volley post request with parameters
                val request = JsonObjectRequest(
                    Request.Method.POST, url, jsonObject,
                    Response.Listener { response ->
                        // Process the json
                        try {
                            Log.i("Reponse", "Response: $response")
                            findNavController().navigate(R.id.action_RegisterFragment_to_RegistrationDone)

                        } catch (e: Exception) {
                            Log.e("Exception error", "Exception: $e")
                            fun Context.toast(message: CharSequence) =
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                        }

                    }, Response.ErrorListener {
                        // Error in request
                        Log.e("Error", "Volley error: $it")

                    })


                // Volley request policy, only one time request to avoid duplicate transaction
                request.retryPolicy = DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                    // 0 means no retry
                    0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
                    1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )

                // Add the volley post request to the request queue
                VolleySingleton.getInstance(this).addToRequestQueue(request)

            } else {
                val text = "Please fill the proper fields in order to register"
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(context, text, duration)
                toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER, 0, 0)

                toast.show()
            }


        }
    }
}

