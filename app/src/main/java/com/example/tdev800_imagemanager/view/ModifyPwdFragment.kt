package com.example.tdev800_imagemanager.view

import UserModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.tdev800_imagemanager.R
import kotlinx.android.synthetic.main.modify_password.*

class ModifyPwdFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.modify_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = ViewModelProviders.of(requireActivity()).get(UserModel::class.java)
        model.username.observe(viewLifecycleOwner,
            Observer<Any> { o -> usernameMod.text = o?.toString() })

        model.emailaddress.observe(viewLifecycleOwner,
            Observer<Any> { o -> emailadressMod.text = o?.toString() })

        //back btn
        view.findViewById<Button>(R.id.backButton).setOnClickListener {
            findNavController().navigate(R.id.action_modify_password_to_user_information)
        }


    }
}